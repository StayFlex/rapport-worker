# Rapport-Worker


## Name
Documentation Worker

## Description
Stage projet Arkindex, le but de ce stage est de réaliser la documentation nécessaire pour qu'un développeur puisse créer un worker, dans l'optique de pouvoir entrainer le modèle d'une IA.


## Installation

Copier le fichier initWorker.sh et requirements.txt dans le dossier dans lequel vous voulez développer vos worker et lancer initWorker.sh

Le fichier bash permet l'initialisation plus rapide d'un worker 

En cas de changement de répertoire de développement, commenter ou supprimmer les 3 dernières lignesdu fichier ~/.bashrc (root)




## Author
Yacine HBADA


## Project status
Etat (Stage): Terminé
