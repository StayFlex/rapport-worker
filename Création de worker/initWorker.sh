#!/bin/bash

#Vérifie que se soit la bonne version de Python
VerifVersionPython(){
    version=$(python3 --version)
    update=$(echo $version | cut -d" " -f2 | cut -d"." -f1)
    patch=$(echo $version | cut -d" " -f2 | cut -d"." -f2)
   
    if [[ $update -ge 3 && $patch -ge 10 ]]; then
        if [[ $patch -eq 11 ]]; then
            apt install python3.11-venv
        fi
        return 1
    else
        return 0
    fi
}
echo "Vous devez être connectez en tant que root pour éxecuter ce script sinon certaine commande retourneront des erreurs!!!"
do
    echo "Etes vous connectez en tant que root? (y/n): "
    read res

    if [[ $res="n" ]]; then
        exit 1;
    fi
done
while [[ $res="y" ]] # Vérification de la condition à la fin

echo "Initialisation de l'environnement de travail"


# Python >=3.10.X
if VerifVersionPython; then
    echo "Mise à jour du python vers une version compatible"
    apt update
    apt install python3
fi

apt install python3-pip python3-virtualenv

echo "Version de python compatible"

# Créer l'environnement virtuel Python s'il n'existe pas déjà
if [! -d "venv" ]; then
    python3 -m venv venv

    echo 'export WORKON_HOME=$HOME/.virtualenvs' >> ~/.bashrc
    echo "export VIRTUALENVWRAPPER_PYTHON=\$(pwd)/venv/bin/python3" >> ~/.bashrc
    echo "source \$(pwd)/venv/bin/virtualenvwrapper.sh" >> ~/.bashrc
fi

source venv/bin/activate

if [[ "$VIRTUAL_ENV"!="" ]] then
    echo "Entrer dans l'envirronnement virtuel"
else
    echo "Echec Entrer dans l'envirronnement virtuel"
    exit 1;
fi

source ~/.bashrc

#lancé :
#pip3 install -q pre-commit
#pip3 install -q tox
#pip3 install -q cookiecutter
#pip3 install -q virtualenvwrapper
pip3 install -qr requirements.txt

echo "Initialisation de l'environnement terminer"

echo "Initialisation du worker"

cookiecutter https://gitlab.teklia.com/workers/base-worker.git


workerName=$(ls -tdd./*/ | grep 'worker-*' | head -n 1)

# Vérifier si workerName a été trouvé
if [ -z "$workerName" ]; then
    echo "Aucun répertoire correspondant trouvé."
else
    # Changer de répertoire vers le répertoire trouvé
    cd "$workerName"

    # Afficher le nouveau répertoire courant
    echo "Répertoire courant : $(pwd)"
fi

echo "Entrer le nom de votre espace de travail"
read  nom
mkvirtualenv $nom

#workon nom de l'envirronement

workon $nom

echo "# Les 3 lignes supérieur sont relié à l'espace $nom" >> ~/.bashrc

pip3 install -q pre-commit
pip3 install -q tox

echo "Mise à niveau des outils $nom"
pip install -e .

#Variable d'envirronement
echo "Entrer l'url de l'instance Arkindex (https://demo.arkindex.org/api/v1/):"
read url
if [[ $url=="" ]]; then
    url="https://demo.arkindex.org/api/v1/"
fi


echo "Entrer le Token de votre compte:"
read token
if [[ $token=="" ]]; then
    token="xxxxxxxxxxxxxxxxxxxxx"
fi

export ARKINDEX_API_URL="$url"
export ARKINDEX_API_TOKEN="$token"

echo "Worker terminer: Ajouter vos modifications dans le fichier worker.py"
echo "Pensez à vérifier que les variables d'envirronement ARKINDEX_API_URL et ARKINDEX_API_TOKEN ne sont pas vide en cas d'erreur"
echo "Entrer dans $workerName et lancer : workon $nom" 
